﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace HostServiceSeed
{
  class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main(string[] args)
    {
      Program p = new Program();
      p.Run(args);
    }

    private void Run(string[] args)
    {
      AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
      try
      {
        if (args.Length != 0 && (args[0].StartsWith(@"/d") || args[0].StartsWith(@"-d")))
        {
          Console.WriteLine("press enter to start service");
          Console.ReadLine();
          var debug = new Service1();
          debug.RunAsApp(args);
          Console.WriteLine("press enter to exit");
          Console.ReadLine();
          debug.StopAsApp();
          Environment.Exit(0);
        }
        else
        {
          ServiceBase[] ServicesToRun;
          ServicesToRun = new ServiceBase[] { new Service1() };
          ServiceBase.Run(ServicesToRun);
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        Console.ReadLine();
      }
    }

    private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      //todo write 2 log
      throw new NotImplementedException();
    }
  }
}
