﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using WebBase;

namespace HostServiceSeed
{
  public partial class Service1 : ServiceBase
  {
    private ServiceHost host = null;
    public Service1()
    {
      InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
      string sIp = ConfigurationManager.AppSettings.AllKeys.Contains("localIP")
          ? ConfigurationManager.AppSettings["localIP"]
          : Dns.GetHostEntry(Dns.GetHostName())
            .AddressList.Where(f => f.AddressFamily == AddressFamily.InterNetwork)
            .Select(x => x.ToString())
            .FirstOrDefault(v => v.StartsWith("10."));
      host = ServiceHostUtils.CreateRegularHost(null/*typeof(WService)*/, null/*typeof(IWebContract)*/,
        string.Format(@"http://{0}/{1}", sIp, ConfigurationManager.AppSettings["serviceAddress"]));
      host.Open();
    }

    protected override void OnStop()
    {

    }

    public void RunAsApp(string[] args)
    {
      this.OnStart(args);
    }

    public void StopAsApp()
    {
      this.OnStop();
    }
  }
}
