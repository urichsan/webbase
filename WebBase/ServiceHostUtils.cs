﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WebBase
{
  public static class ServiceHostUtils
  {
    public static bool AddRegularServiceMethadata(this ServiceHost host, string hostAddress=null)
    {
      try
      {
        //methadata
        ServiceMetadataBehavior smb = host.Description.Behaviors.Find<ServiceMetadataBehavior>();
        if (smb == null)
          smb = new ServiceMetadataBehavior();
        smb.HttpGetEnabled = true;
        smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
        //smb.HttpsGetEnabled = true;
        host.Description.Behaviors.Add(smb);
        host.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexHttpBinding(), hostAddress ?? String.Empty + @"/mex");
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static ServiceHost CreateRegularHost(Type serviceClass, Type serviceInterface, string hostBaseAddress)
    {
      var host = new ServiceHost(serviceClass, new Uri(hostBaseAddress));
      host.AddRegularServiceMethadata();
      //<endpoint address="" binding="webHttpBinding" bindingConfiguration="webHttp" behaviorConfiguration="webHttpBehavior" contract="EditorTestService.IWebContract" />
      ServiceEndpoint webHttpEndpoint = host.AddServiceEndpoint(serviceInterface, GetWebHttpBinding(), hostBaseAddress??String.Empty);
      //<endpointBehaviors><behavior name="webHttpBehavior"><webHttp />
      var webHttpBehavior = new WebHttpBehavior
      {
        AutomaticFormatSelectionEnabled = true,
        HelpEnabled = true,//!string.IsNullOrEmpty(hostAddress)
      };
      
      webHttpEndpoint.Behaviors.Add(webHttpBehavior);
      webHttpEndpoint.Behaviors.Add(new DispatcherSynchronizationBehavior());
      return host;
    }

    public static WebHttpBinding GetWebHttpBinding(WebHttpSecurityMode httpSecurityMode=WebHttpSecurityMode.None,int openTimeSec = 30,int closeTimeSec = 30,int reciveTimeoutSec = 300,int sendTimeoutSec = 300,TransferMode transferMode = TransferMode.Streamed)
    {
      WebHttpBinding webHttpBinding = new WebHttpBinding(httpSecurityMode);//<security mode="None" />
      //openTimeout="00:00:30" closeTimeout="00:00:30"
      webHttpBinding.CloseTimeout = TimeSpan.FromSeconds(openTimeSec);
      webHttpBinding.OpenTimeout = TimeSpan.FromSeconds(closeTimeSec);
      //receiveTimeout="00:05:00" sendTimeout="00:05:00"
      webHttpBinding.ReceiveTimeout = TimeSpan.FromSeconds(reciveTimeoutSec);
      webHttpBinding.SendTimeout = TimeSpan.FromSeconds(sendTimeoutSec);
      //maxBufferPoolSize="2147483647" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647"
      webHttpBinding.MaxBufferPoolSize = Int32.MaxValue;
      webHttpBinding.MaxBufferSize = Int32.MaxValue;
      webHttpBinding.MaxReceivedMessageSize = Int32.MaxValue;
      //transferMode="StreamedResponse"
      webHttpBinding.TransferMode = transferMode;
      //<readerQuotas maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" maxNameTableCharCount="2147483647" maxStringContentLength="2147483647" />
      webHttpBinding.ReaderQuotas.MaxArrayLength = webHttpBinding.ReaderQuotas.MaxBytesPerRead = webHttpBinding.ReaderQuotas.MaxDepth = webHttpBinding.ReaderQuotas.MaxNameTableCharCount = webHttpBinding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
      return webHttpBinding;
    }
  }
}