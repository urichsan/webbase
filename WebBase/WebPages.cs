﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WebBase
{
  [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
  public interface IWebPagesBase
  {
    [OperationContract]
    [WebInvoke(UriTemplate = "/scripts/{fileName}", Method = "GET")]
    Stream Scripts(string fileName);

    [OperationContract]
    [WebInvoke(UriTemplate = "/styles/{fileName}", Method = "GET")]
    Stream Styles(string fileName);

    [OperationContract]
    [WebInvoke(UriTemplate = "/polymer/{dirName}/{fileName}", Method = "GET")]
    Stream Polymer(string dirName, string fileName);

    [OperationContract]
    [WebInvoke(UriTemplate = "/polymer/{dirName}/{subdirName}/{fileName}", Method = "GET")]
    Stream PolymerSubdir(string dirName, string subdirName, string fileName);
  }

  [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
  public interface IWebPages : IWebPagesBase
  {
    [OperationContract]
    [WebInvoke(UriTemplate = "/{fileName}", Method = "GET")]
    Stream Pages(string fileName);
  }

  [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
  public interface IWebPagesSecure : IWebPagesBase
  {
    [OperationContract]
    [WebInvoke(UriTemplate = "/{fileName}?key={keyParam}", Method = "GET")]
    Stream KeyPages(string fileName, string keyParam);
  }

  public class WebPagesBase : IWebPagesBase
  {
    public Stream Scripts(string fileName)
    {
      return GetStreamFromPageFile(@"Scripts\", fileName, @"text/javascript");
    }

    public Stream Styles(string fileName)
    {
      return GetStreamFromPageFile(@"Styles\", fileName, @"text/css");
    }

    public Stream Polymer(string dirName, string fileName)
    {
      return GetStreamFromPageFile(String.Format(@"Polymer\{0}\", dirName), fileName);
    }

    public Stream PolymerSubdir(string dirName, string subdirName, string fileName)
    {
      return GetStreamFromPageFile(String.Format(@"Polymer\{0}\{1}", dirName, subdirName), fileName);
    }

    private MemoryStream GetStreamFromPageFile(string filePath, string fileName, string contentType = @"text/html")
    {
      if (WebOperationContext.Current != null)
      {
        WebOperationContext.Current.OutgoingResponse.ContentType = contentType + "; charset=utf-8";
      }
      try
      {
        byte[] fileData = File.ReadAllBytes(Path.Combine(string.Format(@"{0}\Web\{1}", AppDomain.CurrentDomain.BaseDirectory, filePath), string.IsNullOrEmpty(fileName) ? "empty.file" : fileName));
        return new MemoryStream(fileData);
      }
      catch (Exception ex)
      {
        if (WebOperationContext.Current != null)
          WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
        return new MemoryStream(Encoding.UTF8.GetBytes(ex.Message));
      }
    }

    public class WebPages : WebPagesBase, IWebPages
    {
      public Stream Pages(string fileName)
      {
        return GetStreamFromPageFile(@"Pages\", fileName + (fileName.EndsWith(".ico") ? "" : ".html"));
      }
    }
  }
}
